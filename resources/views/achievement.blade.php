<!-- Projects-->
@extends('biodata.index')
@section('container')
<section class="projects-section bg-light" id="projects">
            <div class="container px-4 px-lg-5">
                <!-- Featured Project Row-->
                <div class="row gx-0 mb-4 mb-lg-5 align-items-center">
                    <div class="col-xl-7 col-lg-8"><img class="img-fluid mb-3 mb-lg-0" src="{{asset('template/assets/img/runnerupmlcc.jpeg')}}" alt="..." width="500" /></div>
                    <div class="col-xl-4 col-lg-5">
                        <div class="featured-text text-center text-lg-left">
                            <h4>Juara 2 Mobile Legend Campus Championship Indonesia</h4>
                            <p class="text-black-50 mb-0">Tournamen MLCC ini merupakan pertandingan Mobile Legend tingkat kampus yang diadakan oleh pihak Developernya langsung, Tujuan di adakan tournamen ini untuk memunculkan bibit-bibit / talenta pemain Esport di Indonesia. Pada Tournamen MLCC ini saya mendapatkan Juara 2 pada babak kualifikasi yang dilakukan di kampus undiksha.</p>
                        </div>
                    </div>
                </div>
                <div class="row gx-0 mb-4 mb-lg-5 align-items-center">
                    <div class="col-xl-7 col-lg-8"><img class="img-fluid mb-3 mb-lg-0" src="{{asset('template/assets/img/champions.jpeg')}}" alt="..." width="500" /></div>
                    <div class="col-xl-4 col-lg-5">
                        <div class="featured-text text-center text-lg-left">
                            <h4>Juara 1 Dies Natalis Undiksha</h4>
                            <p class="text-black-50 mb-0">Dies Natalis merupakan perayaan ulang tahun Universitas Pendidikan Ganesha , dalam kegiatan Dies Natalis ini banyak lomba yang diselenggarakan seperti lomba PKM , Lomba Futsal , E-Sport dll , Pada Dies Natalis ini saya sebagai konten dari FTK mengikuti Lomba E-Sport Mobile Legend dan pada lomba ini saya mendapatkan Juara 1 dan Mendapatkan Mendali Emas Untuk FTK. </p>
                        </div>
                    </div>
                </div>
                <div class="row gx-0 mb-4 mb-lg-5 align-items-center">
                    <div class="col-xl-7 col-lg-8"><img class="img-fluid mb-3 mb-lg-0" src="{{asset('template/assets/img/juara2pat.jpeg')}}" alt="..." width="500" /></div>
                    <div class="col-xl-4 col-lg-5">
                        <div class="featured-text text-center text-lg-left">
                            <h4>Juara 2 Mobile Legend Pada Pagelaran Akhir Tahun (PAT) FTK UNDIKSHA</h4>
                            <p class="text-black-50 mb-0">Tournamen Mobile Legend Kategori Umum pada pagelaran akhir tahun (PAT) FTK Undiksha, PAT ini merupakan acara Ulang Tahun FTK Undiksha dimana dalam kegiatan Pagelaran Akhir Tahun (PAT) ini banyak sekali lomba yang diadakan seperti lomba pkm , musikalisasi puisi , karaoke hingga Lomba E-sport. Pada Pagelaran Akhir Tahun kategori lomba E-Sport Mobile Legend Saya Mendapatkan Juara 2 dari 64 peserta yang mengikuti Tournamen ini </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection