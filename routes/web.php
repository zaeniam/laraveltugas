<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;
use App\Http\Controllers\aboutController;
use App\Http\Controllers\achievementController;
use App\Http\Controllers\contacController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [homeController::class, 'index']);

Route::get('/home', [homeController::class, 'index']);

Route::get('/about', [aboutController::class, 'index']);

Route::get('/achievement', [achievementController::class, 'index']);

Route::get('/contac', [contacController::class, 'index']);
